package com.springapp.mvc;

import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;

@Controller
@RequestMapping("/")
public class HelloController {
	@RequestMapping(method = RequestMethod.GET)
	public String printWelcome(ModelMap model) {
		model.addAttribute("message", "Hello world!");
		return "hello";
	}

	@RequestMapping(value = "/form", method = RequestMethod.POST)
	public String handleFormUpload(@RequestParam("name") String name,
								   @RequestParam("file") MultipartFile file) throws IOException {
		System.out.println("Content type ---> " + file.getContentType());
		System.out.println("Name ---> " + file.getName());
		System.out.println("Original filename ---> " + file.getOriginalFilename());
		System.out.println("Size ---> " + file.getSize());

		if (!file.isEmpty()) {
			byte[] bytes = file.getBytes();
			// store the bytes somewhere
			return "uploadSuccess";
		}

		return "uploadFailure";
	}
}